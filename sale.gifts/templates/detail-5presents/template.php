<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<? if(count($arResult["GIFTS"]) > 0):?>
<section class="rasg-section detail-5presents">
	<span class="site-h2-h3-tt"><i class="fa fa-gift"></i>&ensp;<?=Loc::getMessage("RASG_TITLE");?></span>

	<?/* Вывод найденных подарков */?>
	<div class="rasg-block">
		<ul class="rasg-list">
			<?
			$i = 1; 
			$m = count($arResult["GIFTS"]);
			foreach($arResult["GIFTS"] as $arGift):?>
				<li class="rasg-item prod-item">
					<p class="condition prod-tag-discount">
						<?if( !($arGift["BASKET_SUM"]["logic"] == "Great" && $arGift["BASKET_SUM"]["Value"] == 0) ):?>
							<?=$arGift["BASKET_SUM"]["HTML"];?>
						<?endif;?>
					</p>
					<a href="<?=$arGift["ITEM"]["Products"]["DETAIL_PAGE_URL"];?>" class="prod-img"><img src="<?=$arGift["ITEM"]["Products"]["PICTURE"]["src"];?>"></a>
					<a href="<?=$arGift["ITEM"]["Products"]["DETAIL_PAGE_URL"];?>" class="prod-name"><?=$arGift["ITEM"]["Products"]["NAME"];?></a>
				</li>
				<?if($i < $m):?>
					<li class="plus">+</li>
				<?endif?>
				<?$i++;?>
			<?endforeach;?>
		</ul>
	</div>
</section>
<?endif;?>


