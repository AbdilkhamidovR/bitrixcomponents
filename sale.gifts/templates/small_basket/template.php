<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>

<? if( is_array($arResult["GIFTS"]) && count($arResult["GIFTS"]) > 0 ):?>
	<p class="h-cart-name"><i class="fa fa-gift"></i> Ваши подарки</p>
	<p class="gift-list">
	<?foreach ($arResult["GIFTS"] as $arGift){ 
		$arStr[] = '<a href="'.$arGift["DETAIL_PAGE_URL"].'">'.$arGift["NAME"].'</a>';
	}
	echo implode(", ", $arStr);
	?>
	</p>
	<p class="conditions"><a href="<?=$arParams["CONDITIONS_LINK"];?>">Условия получения подарков</a></p>
<?endif;?>


