<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>

<? if( is_array($arResult["GIFTS"]) && count($arResult["GIFTS"]) > 0 ):?>
	<section class="rasg-section big-basket-template">
		<?/* Вывод найденных подарков */?>
		<div class="rasg-header aside-info-tt-wrapp group">
			<span class="site-h2-tt"><i class="fa fa-gift"></i>&ensp;<?=Loc::getMessage("RASG_TITLE");?></span>
			<div class="aside-info-tt-head">
				<div class="aside-info-link-arr">
					<a href="<?=$arParams["CONDITIONS_LINK"];?>">
						<span><?=Loc::getMessage("RASG_GIFT_CONDITIONS");?></span>
						<i class="icon icon-arr-l"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="rasg-block">
			<ul class="rasg-list">
				<?
				$w = floor(100/count($arResult["GIFTS"]));
				foreach($arResult["GIFTS"] as $arGift):?>
					<li class="rasg-item prod-item" style="width:<?=$w;?>%">
						<a href="<?=$arGift["DETAIL_PAGE_URL"];?>" class="prod-img"><img src="<?=$arGift["PICTURE"]["src"];?>"></a>
						<a href="<?=$arGift["DETAIL_PAGE_URL"];?>" class="prod-name"><?=$arGift["NAME"];?></a>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</section>
<?endif;?>


