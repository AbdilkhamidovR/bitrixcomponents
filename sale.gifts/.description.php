<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("RASG_NAME"),
	"DESCRIPTION" => GetMessage("RASG_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "RA",
		"CHILD" => array(
			"ID" => "sale",
			"NAME" => GetMessage("T_IBLOCK_DESC_CATALOG"),
			"SORT" => 30
		),
		"NAME" => GetMessage("RASG_NAME")
	),
);

?>