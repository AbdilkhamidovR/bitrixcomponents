<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Iblock\IblockTable;

Loader::includeModule('iblock');

$rsIBlock = \Bitrix\Iblock\IblockTable::getList(array(
	'filter' => array("=ACTIVE" => "Y", "=IBLOCK_TYPE_ID" => "catalog"),
	'select' => array('ID', 'NAME'),
));
while ($arr = $rsIBlock->Fetch()){
	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
}
unset($arr, $rsIBlock);


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("RASG_IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"DISCOUNT_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("RASG_DISCOUNT_CODE"),
			"TYPE" => "STRING",
			"DEFAULT" => "GIFT",
		),
		"DISCOUNTS_LIMIT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("RASG_DISCOUNTS_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => "4",
		),
		"LIMIT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("RASG_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
	),
);
?>
