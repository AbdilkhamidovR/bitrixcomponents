<?

namespace Ra\Sale\Gifts;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Sale\Internals\DiscountTable as DiscountTable;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Currency\CurrencyLangTable;
use Bitrix\Main\Context;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Fuser;
use Bitrix\Sale\Internals\BasketTable;
use Bitrix\Sale\Internals\BasketPropertyTable;

Loader::includeModule('sale');
Loader::includeModule('iblock');

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


Loc::loadMessages(__FILE__);

/* загрузка классов */
// spl_autoload_register(function ($class) {
// 	include_once 'lib/questionform.class.php';
// });

// error_reporting(E_ALL);



/**
 * Class RaSaleOfferFaq
 * @package Ra\Sale\Gifts
 */
class RaSaleGifts extends \CBitrixComponent{

	protected $arSort = array();
	protected $arFilter = array();

	public function onPrepareComponentParams($arParams){

		$this->arResult["ERRORS"] = array();

		// ppr($arParams, __FILE__.' $arParams');

		$this->processRequest();

		return $arParams;
	}


	/**
	 * Обработка запроса
	 * 
	 */
	protected function processRequest(){
		$this->arResult["POST"] = $this->request->getPostList()->toArray();
		return $this->arResult["POST"];
	}


	/**
	 * Получение подарков
	 * 
	 */
	protected function getAllGifts(){
		$arDiscounts = DiscountTable::getList(array(
			'filter' => array(
					'=XML_ID' => $this->arParams["DISCOUNT_CODE"],
				),
			'order' => array("SORT" => "ASC"),
			'limit' => $this->arParams["DISCOUNTS_LIMIT"],
			'select' => array("*"),
		))->fetchAll();

		$arGifts = array();
		foreach ($arDiscounts as $arDiscount) {
			$arGift = array();
			if($arDiscount["CONDITIONS_LIST"]["CLASS_ID"] == "CondGroup"){
				foreach ($arDiscount["CONDITIONS_LIST"]["CHILDREN"] as $key => $arChildren) {
					if($arChildren["CLASS_ID"] == "CondBsktAmtGroup"){ 
						// "CondBsktAmtGroup" - условие "Общая стоимость товаров..." 
						$arGift["BASKET_SUM"][ $arChildren["DATA"]["logic"] ] = $arChildren["DATA"];
						$arGift["BASKET_SUM"][ $arChildren["DATA"]["logic"] ]["HTML"] = $this->getConditionHtml($arChildren["DATA"]);
					}
				}
			}
			if($arDiscount["ACTIONS_LIST"]["CLASS_ID"] == "CondGroup" && 
				$arDiscount["ACTIONS_LIST"]["CHILDREN"][0]["CLASS_ID"] == "GiftCondGroup")
			{
				foreach ($arDiscount["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"] as $key => $arChildren) {
					if($arChildren["CLASS_ID"] == "GifterCondIBElement"){ 
						// "CondBsktAmtGroup" - условие "Общая стоимость товаров..." 
						foreach ($arChildren["DATA"]["Value"] as $key => $value) {
							$arGift["ITEMS"][] = array(
								"Type" => $arChildren["DATA"]["Type"],
								"Products" => $this->getProductData($value)
							);
						}
					}
				}
			}
			$arGifts[] = $arGift;
		}
		return $arGifts;
	}


	/**
	 * Обновление подарков в корзине
	 * 
	 */
	protected function updateGiftsInBasket(){

		$arAllGifts = $this->getAllGifts();

		$basket = Basket::loadItemsForFUser(Fuser::getId(), Context::getCurrent()->getSite());

		// очищаем корзину от подарков
		$this->deleteGiftsFromBasket($basket);

		// костыль для получения цены со скидками в $basket->getPrice() >>
		// взял тут: https://dev.1c-bitrix.ru/support/forum/forum6/topic98143/
		$order = \Bitrix\Sale\Order::create("s1", Fuser::getId());
		$order->setPersonTypeId(1);
		$order->setBasket($basket);
		$order->getDiscount()->getApplyResult();
		// <<

		// добавляем подарки в корзину
		$arGifts = array();
		if($basket->getPrice() > 0){
			foreach($arAllGifts as $arGift){
				if(
					(
						( isset($arGift["BASKET_SUM"]["EqGr"]["Value"]) && $basket->getPrice() >= $arGift["BASKET_SUM"]["EqGr"]["Value"] )
						|| 
						( isset($arGift["BASKET_SUM"]["Great"]["Value"]) && $basket->getPrice() > $arGift["BASKET_SUM"]["Great"]["Value"] )
					)
					&&
					(
						( isset($arGift["BASKET_SUM"]["EqLs"]["Value"]) && $basket->getPrice() <= $arGift["BASKET_SUM"]["EqLs"]["Value"] ) 
						|| 
						( isset($arGift["BASKET_SUM"]["Less"]["Value"]) && $basket->getPrice() < $arGift["BASKET_SUM"]["Less"]["Value"] )
					)
				){
					foreach ($arGift["ITEMS"] as $arItem) {
						$arGifts[] = $arItem["Products"];
						$this->addGifts2Basket($basket, $arItem["Products"]);
					}
				}
			}
		}
		return $arGifts;
	}


	/**
	 * Добавление подарков в корзину
	 * 
	 */
	protected function addGifts2Basket($basket, $arItem){
		$productId = $arItem["ID"];

		if ( !($basketItem = $basket->getExistsItem('catalog', $productId)) ) {

			$basketItem = $basket->createItem('catalog', $productId);
			$arFields = array(
				'QUANTITY' => 1,
				'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
				'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				'PRICE' => 0,
			);
			$arFields["NAME"] = html_entity_decode($arItem["NAME"]);
			// for 1C 
			$arFields['CATALOG_XML_ID'] = $arItem["IBLOCK_EXTERNAL_ID"]; // TO DO
			$arFields['PRODUCT_XML_ID'] = $arItem["EXTERNAL_ID"];

			$basketItem->setFields($arFields);
			$basketItem->save();

			# add properties
			$properties = array(
				array(
					"NAME" => "Подарок",
					"CODE" => "IS_GIFT",
					"VALUE" => "Y",
					'SORT' => 100
				),
				array(
					"NAME" => "Catalog XML_ID",
					"CODE" => "CATALOG.XML_ID",
					"VALUE" => $arItem["IBLOCK_EXTERNAL_ID"],
					'SORT' => 200
				),
				array(
					"NAME" => "Product XML_ID",
					"CODE" => "PRODUCT.XML_ID",
					"VALUE" => $arItem["EXTERNAL_ID"],
					'SORT' => 300
				),
			);
			if($properties) {
				$propertyCollection = $basketItem->getPropertyCollection();
				$propertyCollection->setProperty($properties);
				$propertyCollection->save();
			}
		}
	}


	/**
	 * получение товаров корзины со свойствами
	 * 
	 */
	protected function getBasketItems($basket){
		$arBasketItems = array();
		foreach ($basket as $basketItem) {
			$propertyCollection = $basketItem->getPropertyCollection();
			$arBasketItems[] =  array(
				"NAME" => $basketItem->getField('NAME'),
				"BASKET_ID" => $basketItem->getId(),
				"PRODUCT_ID" => $basketItem->getProductId(),
				"DETAIL_PAGE_URL" => $basketItem->getField('DETAIL_PAGE_URL'),
				"QUANTITY" => $basketItem->getQuantity(),
				"PRICE" => $basketItem->getPrice(),
				"SUM" => $basketItem->getFinalPrice(),
				"CATALOG_XML_ID" => $basketItem->getField('CATALOG_XML_ID'),
				"PRODUCT_XML_ID" => $basketItem->getField('PRODUCT_XML_ID'),
				"PRODUCT_PROVIDER_CLASS" => $basketItem->getField('PRODUCT_PROVIDER_CLASS'),
				"PROPS" => $propertyCollection->getPropertyValues(),
			);
		}
		return $arBasketItems;
	}


	/**
	 * получение подарков в корзине
	 * 
	 */
	protected function getGiftsInBasket(){

		$arGifts = \Bitrix\Sale\Basket::getList(array(
			'filter' => array(
				'=FUSER_ID' => \Bitrix\Sale\Fuser::getId(), 
				'=ORDER_ID' => null,
				'=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
				'=CAN_BUY' => 'Y',
				array(
					'LOGIC' => 'AND',
					'=PROPERTY_CODE' => 'IS_GIFT',
					'=PROPERTY_VALUE' => 'Y',
				)
			),
			'select' => array('NAME', 'ID', 'PRODUCT_ID', 'DETAIL_PAGE_URL', 'QUANTITY', 'PRICE', 'DISCOUNT_PRICE', 'BASE_PRICE', 'CATALOG_XML_ID',  'PRODUCT_XML_ID', 'PRODUCT_PROVIDER_CLASS',
				'PROPERTY_CODE' => 'PROPERTY_PROP.CODE', 'PROPERTY_VALUE' => 'PROPERTY_PROP.VALUE',
				// 'PROPERTY_PROP'
			),
			'runtime' => array(
				new \Bitrix\Main\Entity\ReferenceField(
					'PROPERTY_PROP',
					'\Bitrix\Sale\Internals\BasketPropertyTable',
					array(
						'=this.ID' => 'ref.BASKET_ID',
						),
					array('join_type' => 'LEFT')
				),
			),
		))->fetchAll();

		foreach ($arGifts as &$arGift) {
			$arGift = array_merge($arGift, $this->getProductData($arGift["PRODUCT_ID"]));
		}
		return $arGifts;
	}


	/**
	 * очистка корзины от подарков
	 * 
	 */
	protected function deleteGiftsFromBasket($basket){
		foreach ($basket as $basketItem) {
			$arProps = $basketItem->getPropertyCollection()->getPropertyValues();
			if( isset($arProps["IS_GIFT"]) && $arProps["IS_GIFT"]["VALUE"] == "Y" ){
				$basketItem->delete();
			}
		}
		$basket->save();
	}



	/**
	 * Формирование html-кода условия цены
	 * 
	 */
	protected function getConditionHtml($arCondition){
		switch ($arCondition["logic"]) {
			case 'Great':
			case 'EqGr':
				$str = "от ";
				break;
			default:
				break;
		}
		$strPrice = str_replace("#", $arCondition["Value"], $this->getPriceFormat($arCondition));
		$conditionHtml = $str.$strPrice;
		return $conditionHtml;
	}

	/**
	 * Формирование html-кода условия цены
	 * 
	 */
	protected function getPriceFormat($arCondition){
		$arCurrency = \Bitrix\Currency\CurrencyLangTable::getRow(array(
			'filter' => array(
					'=LID' => 'ru',
					'=CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				),
			'select' => array("CURRENCY", "FORMAT_STRING")
		));
		return $arCurrency["FORMAT_STRING"];
	}


	/**
	 * Получение данных товара
	 * 
	 */
	protected function getProductData($id){
		// Выборка элементов
		$arProduct = \CIBlockElement::GetList(
			array("CREATED"=>"DESC"), 
			array('=IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
					'=ID' => $id), 
			false, 
			false,
			array("IBLOCK_ID", "ID", "NAME", 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID')
		)->GetNext(true, false);
		
		$pictureId = $arProduct['PREVIEW_PICTURE'] ? $arProduct['PREVIEW_PICTURE'] : $arProduct['DETAIL_PICTURE'];
		$arProduct["PICTURE"] = \CFile::ResizeImageGet(
			$pictureId,
			array("width" => 210, "height" => 190),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true,
			array()
		);
		return $arProduct;
	}


	/**
	 * Получение данных элемента корзины
	 * 
	 */
	protected function getBasketData(){
		$basket = Basket::loadItemsForFUser(Fuser::getId(), Context::getCurrent()->getSite());
		$arBasketData = array(
			"PRICE" => $basket->getPrice(),
			"FUSER" => $basket->getFUserId($skipCreate = false),
		);
		return $arBasketData;
	}




	public function getResult(){
		return $this->arResult;
	}


	/**
	 * Execute component
	 * 
	 */
	public function executeComponent(){

		try {

			// fppr($this->arParams, __FILE__.' executeComponent: $this->arParams');

			$this->arResult["STATUS"] = $this->arResult["ERRORS"] ? "FAIL" : "OK";

			// fppr($this->arResult, __FILE__.' $this->arResult', false, false);
			
			switch ($this->arParams["ACTION"]) {
				case 'updateGifts':
					$this->arResult["GIFTS"] = $this->updateGiftsInBasket();
					break;

				case 'getGiftsInBasket':
					$this->arResult["GIFTS"] = $this->getGiftsInBasket();
					break;
				
				default:
					$this->arResult["GIFTS"] = $this->getAllGifts();
					break;
			}

			switch ($this->arParams["RESULT_TYPE"]) {
				case 'json':
					echo json_encode($this->arResult);
					break;
				case 'array':
					return $this->arResult;
					break;
				case 'html':
				default:
					$this->includeComponentTemplate();
					break;
			}
			
		} catch (Exception $e) {

			$this->errorsFatal[htmlspecialcharsEx($e->getCode())] = htmlspecialcharsEx($e->getMessage());

			echo $e->getMessage();
			
		}
	}
}
?>
