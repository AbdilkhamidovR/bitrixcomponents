<?

namespace Ra\Sale\Offer\Faq;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Loader;
Loader::includeModule('iblock');

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


Loc::loadMessages(__FILE__);

/* загрузка классов */
spl_autoload_register(function ($class) {
	include_once 'lib/questionform.class.php';
	include_once 'lib/search.class.php';
});

// error_reporting(E_ALL);



/**
 * Class RaSaleOfferFaq
 * @package Ra\Sale\Offer\Faq
 */
class RaSaleOfferFaq extends \CBitrixComponent{

	protected $arSort = array();
	protected $arFilter = array();

	public function onPrepareComponentParams($arParams){

		// ppr($arParams, __FILE__.' $arParams');
		$this->arResult["WARNINGS"] = array();
		$this->arResult["ERRORS"] = array();
		$this->arResult["SUCCESS"] = array();
		
		$this->arResult["SORT"] = array("PROPERTY_Q_LIKES_QUANTITY"=>"DESC", "ID"=>"DESC");
		$this->arResult["FILTER"] = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"], 
			"ACTIVE" => "Y", 
			"=PROPERTY_Q_IS_ANSWERED_VALUE" => "Y", 
		);
		if($arParams["LINKED_ID"]){
			$this->arResult["FILTER"]["=PROPERTY_Q_LINK_TO_ELEMENT"] = $arParams["LINKED_ID"];
		}
		$this->arResult["SELECT"] = array("IBLOCK_ID", "DATE_CREATE", "ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PROPERTY_Q_LINK_TO_ELEMENT", "PROPERTY_Q_DATE_MAIN_ANSWER", "PROPERTY_Q_IS_ANSWERED", "PROPERTY_Q_MORE_ANSWERS", "PROPERTY_Q_DATE_MORE_ANSWERS", "PROPERTY_Q_LIKES_QUANTITY", "PROPERTY_Q_USERS_LIKE_UP", "PROPERTY_Q_USERS_LIKE_DOWN");
		$this->arResult["NAV"] = array("nPageSize" => $arParams["LIMIT"], "iNumPage" => 1);

		$this->getUser();

		return $arParams;
	}


	/**
	 * Обработка запроса
	 * 
	 */
	protected function processRequest(){

		$allowed = '[^a-zа-яё0-9!;= _\(\)\-\.\,\?\"\'\$]'; // допустимые символы в строке поиска, остальные вырезаются
		$patt_replace = "/".$allowed."/iu"; 
		$patt_email = "/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/iu";
		$patt_question = "/.{5,}/iu";


		$this->arResult["REQUEST"] = array(
			"POST_LIST" => $this->request->getPostList(),
			"METHOD" => $this->request->getRequestMethod(),
			"AJAX" => $this->request->isAjaxRequest(),
			"SEARCH_RESET" => $this->request->getPost("search_reset"),
			"SEARCH_SUBMIT" => $this->request->getPost("search_submit"),
			"QUESTION_SUBMIT" => $this->request->getPost("question_submit"),
			"CHECK_AUTH" => $this->request->getPost("check_auth"),
			"LIKE" => $this->request->getPost("like"),
			"LIKE_ID" => $this->request->getPost("id"),
			"PAGEN_1" => (int)$this->request->get("PAGEN_1"),
		);


		// валидация данных с формы поиска
		if($this->arResult["REQUEST"]["METHOD"] == "POST" && $this->arResult["REQUEST"]["SEARCH_SUBMIT"]){
			if(strlen($this->request->getPost("q")) >= 3){
				$this->arResult["REQUEST"]["SEARCH"] = preg_replace($patt_replace, '', $this->request->getPost("q") );
			}else{
				$this->arResult["ERRORS"]["VALIDATION"]["SEARCH_FORM"]["SEARCH"] = array("CODE" => "SRCH_STRLEN_SHORT", "MESS" => Loc::getMessage("SRCH_STRLEN_SHORT") );
				$this->arResult["REQUEST"]["SEARCH"] = "";
			}
		}

		// валидация данных с формы добавления вопроса
		if($this->arResult["REQUEST"]["METHOD"] == "POST" && $this->arResult["REQUEST"]["QUESTION_SUBMIT"] && check_bitrix_sessid()){

			$this->arResult["REQUEST"]["sessid"] = $this->request->getPost("sessid");

			$user_question = htmlentities( trim( $this->request->getPost("question") ) );
			if(!preg_match($patt_question, $user_question, $matches)){
				$this->arResult["ERRORS"]["VALIDATION"]["QUESTION_FORM"]["QUESTION"] = array("CODE" => "VL_QUESTION_NOT_VALID", "MESS" => Loc::getMessage("VL_QUESTION_NOT_VALID") );
			}
			$this->arResult["REQUEST"]["QUESTION"] = $user_question;
		}

		if($this->arResult["REQUEST"]["PAGEN_1"] > 1){
			$this->arResult["NAV"]["iNumPage"] = $this->arResult["REQUEST"]["PAGEN_1"]; 
		}

		return;
	}


	/**
	 * Установка сортировки выборки
	 */
	protected function setOrder(){
		return;
	}



	/**
	 * Установка фильтра выборки
	 */
	protected function setFilter(){
		if(!$this->arResult["REQUEST"]["SEARCH_RESET"]){

			$searchFilter = array(
				"LOGIC" => "OR",
				array("NAME" => "%".$this->arResult["REQUEST"]["SEARCH"]."%"),
				array("DETAIL_TEXT" => "%".$this->arResult["REQUEST"]["SEARCH"]."%"),
			);

			$this->arResult["FILTER"][] = $searchFilter;

		}else{
			unset($this->arResult["REQUEST"]["SEARCH"]);
		}
	}


	/**
	 * Выборка элементов из инфоблока FAQ
	 */
	protected function readItems(){

		$res = \CIBlockElement::GetList(
			$this->arResult["SORT"], 
			$this->arResult["FILTER"], 
			false, 
			$this->arResult["NAV"],
			$this->arResult["SELECT"]
		);

		$this->arResult["ITEMS_QUANTITY"] = $res->SelectedRowsCount();

		if((int)$this->arResult["NAV"]["nPageSize"]){
			$this->arResult["NAV"]["nPages"] = ceil($this->arResult["ITEMS_QUANTITY"] / $this->arResult["NAV"]["nPageSize"]);
		}else{
			$this->arResult["NAV"]["nPages"] = 1;
		}
		
		$this->arResult["ITEMS"] = array();

		$res->NavStart($this->arResult["NAV"]["nPageSize"]);
		
		while($arRes = $res->NavNext(true, "f_")){
			$arRes["PROPERTY_Q_LIKES_QUANTITY_VALUE"] = (int)$arRes["PROPERTY_Q_LIKES_QUANTITY_VALUE"];
			
			$pattern = "/\b".$this->arResult["USER"]["ID"]."\b/i";
			$arRes["IS_USER_LIKED_UP"] = preg_match($pattern, $arRes["PROPERTY_Q_USERS_LIKE_UP_VALUE"]) ? true : false;
			$arRes["IS_USER_LIKED_DOWN"] = preg_match($pattern, $arRes["PROPERTY_Q_USERS_LIKE_DOWN_VALUE"]) ? true : false;

			$this->arResult["ITEMS"][ $arRes["ID"] ] = $arRes;
		}
		// echo $res->NavPrint("Вопросы");
		return;
	}


	/**
	 * Выделение найденной фразы
	 */
	protected function parseContent(){

		if($this->arResult["ITEMS"]){
			foreach ($this->arResult["ITEMS"] as &$arItem) {
				if($this->arResult["REQUEST"]["SEARCH"]){
					$pattern = "/(".$this->arResult["REQUEST"]["SEARCH"].")/iu";
					$arItem["NAME"] = preg_replace($pattern, "<span class='search-request'>\\1</span>", $arItem["NAME"] );
					$arItem["DETAIL_TEXT"] = preg_replace($pattern, "<span class='search-request'>\\1</span>", $arItem["DETAIL_TEXT"] );
				}
			}
		}
	}


	/**
	 * Нравится - Не нравится. Получение значения
	 * 
	 */
	protected function getLikesValue($id){
		$arProp = \CIBlockElement::GetProperty($this->arParams["IBLOCK_ID"], $id, array("sort" => "asc"), array("CODE"=>"Q_LIKES_QUANTITY"))->GetNext(true, false);
		return $arProp["VALUE"];
	}


	/**
	 * Нравится - Не нравится. Установка значения
	 * 
	 */
	protected function setLikesValue($id){

		if($this->arResult["REQUEST"]["LIKE"] && $this->arResult["USER"]){
			$likesQuantity = (int)$this->getLikesValue($this->arResult["REQUEST"]["LIKE_ID"]);
			$likedQuestionId = $this->arResult["REQUEST"]["LIKE_ID"];

			switch ($this->arResult["REQUEST"]["LIKE"]) {
				case '1':
					if(!$this->arResult["ITEMS"][$likedQuestionId]["IS_USER_LIKED_UP"]){
						$likesQuantity += (int)$this->arResult["REQUEST"]["LIKE"];

						$arProps = array(
							"Q_USERS_LIKE_UP" => $this->arResult["ITEMS"][$likedQuestionId]["PROPERTY_Q_USERS_LIKE_UP_VALUE"].','.$this->arResult["USER"]["ID"],
							"Q_USERS_LIKE_DOWN" => str_replace(','.$this->arResult["USER"]["ID"], '', $this->arResult["ITEMS"][$likedQuestionId]["PROPERTY_Q_USERS_LIKE_DOWN_VALUE"]),
							"Q_LIKES_QUANTITY" => $likesQuantity
						);
					}
					break;
					
				case '-1':
					if(!$this->arResult["ITEMS"][$likedQuestionId]["IS_USER_LIKED_DOWN"]){
						$likesQuantity += (int)$this->arResult["REQUEST"]["LIKE"];

						$arProps = array(
							"Q_USERS_LIKE_UP" => str_replace(','.$this->arResult["USER"]["ID"], '', $this->arResult["ITEMS"][$likedQuestionId]["PROPERTY_Q_USERS_LIKE_UP_VALUE"]),
							"Q_USERS_LIKE_DOWN" => $this->arResult["ITEMS"][$likedQuestionId]["PROPERTY_Q_USERS_LIKE_DOWN_VALUE"].','.$this->arResult["USER"]["ID"],
							"Q_LIKES_QUANTITY" => $likesQuantity
						);
					}
					break;
			}

			\CIBlockElement::SetPropertyValuesEx(
				$this->arResult["REQUEST"]["LIKE_ID"], 
				$this->arParams["IBLOCK_ID"], 
				$arProps
			);
		}
	}
	

	/**
	 * Определение пользователя
	 * 
	 */
	protected function getUser(){
		global $USER;
		if ($USER->IsAuthorized()){
			$this->arResult["USER"] = \CUser::GetList(
				$by = 'ID', 
				$order = 'DESC',
				array("ID" => $USER->GetID()),
				array("FIELDS" => array("ID", "EMAIL", "NAME", "LAST_NAME", "SECOND_NAME"))
			) -> GetNext(true, false);
		}else{
			$this->arResult["USER"] = false;
		}
	}


	/**
	 * Добавление вопроса
	 * 
	 */
	protected function addQuestion(){
		$arFileds = array(
			"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
			"IBLOCK_SECTION_ID" => $this->arParams["IBLOCK_SECTION_ID"],
			"MODIFIED_BY" => $this->arResult["USER"]["ID"],
			"NAME" => $this->arResult["REQUEST"]["QUESTION"],
			"PREVIEW_TEXT" => $this->arResult["REQUEST"]["QUESTION"],
			"ACTIVE"  => "N"
		);
		$el = new \CIBlockElement;

		if(!$this->arResult["ADDED_QUESTION"] = $el->add($arFileds)){
			$this->arResult["ERRORS"]["ADDED_QUESTION_ERROR"] = array("CODE" => "ADDED_QUESTION_ERROR", "MESS" => $el->LAST_ERROR);
			return false;
		}

		$el->SetPropertyValuesEx($this->arResult["ADDED_QUESTION"], $this->arParams["IBLOCK_ID"], array("Q_LINK_TO_ELEMENT" => $this->arParams["LINKED_ID"]));

		$el->Update($this->arResult["ADDED_QUESTION"], array("CODE" => $this->arResult["ADDED_QUESTION"]));

		$this->arResult["SUCCESS"]["ADDED_QUESTION"] = array("CODE" => "ADDED_QUESTION_SUCCESS", "MESS" => Loc::getMessage("ADDED_QUESTION_SUCCESS"));

		return $this->arResult["ADDED_QUESTION"];
	}


	/**
	 * Execute component
	 * 
	 */
	public function executeComponent(){

		try {
			// ppr($this->arParams, __FILE__.' executeComponent: $this->arParams');

			// $this->arResult["SEARCH_RESULT"] = Search::getSearchForm();
			// $this->arResult["FORM_RESULT"] = QuestionFrom::get_test();


			$this->processRequest();

			// запрошена проверка авторизации
			if($this->arResult["REQUEST"]["CHECK_AUTH"]){
				if($this->arResult["USER"]){
					echo json_encode( array("USER_ID" => $this->arResult["USER"]["ID"]) );
				}else{
					echo json_encode( array("USER_ID" => false) );
				}
				return;
			}

			$this->setFilter();
			
			$this->readItems();

			$this->parseContent();


			// Изменение лайков
			if($this->arResult["REQUEST"]["LIKE"]){
				if($this->arResult["USER"]){

					$this->setLikesValue($this->arResult["REQUEST"]["LIKE_ID"]);
					
					echo json_encode( array("LIKES_QUANTITY" => (int)$this->getLikesValue($this->arResult["REQUEST"]["LIKE_ID"]), "USER_ID" => $this->arResult["USER"]["ID"]));
					return;
				}else{
					echo json_encode( array("USER_ID" => false) );
					return;
				}
			}


			// Добавление вопроса и возврат ответа
			if($this->arResult["USER"] && 
				$this->arResult["REQUEST"]["QUESTION_SUBMIT"]){

				if($this->arResult["ERRORS"]["VALIDATION"]["QUESTION_FORM"]["QUESTION"]){
					echo json_encode($this->arResult["ERRORS"]["VALIDATION"]["QUESTION_FORM"]["QUESTION"]);
					return;
				}
				
				if($this->addQuestion()){

					// Вопрос успешно добавлен
					// Отправляем его на почту ответственного менеджера
					Event::send(array(
						"EVENT_NAME" => "OFFER_FAQ_NEW_QUESTION_ADDED",
						"LID" => "s1",
						"C_FIELDS" => array(
							"LINKED_ID" => $this->arParams["LINKED_ID"],
							"CREATED_BY_EMAIL" => $this->arResult["USER"]["EMAIL"],
							"QUESTION_ID" =>  $this->arResult["ADDED_QUESTION"],
							"QUESTION" => $this->arResult["REQUEST"]["QUESTION"],
							"IBLOCK_SECTION_ID" => $this->arParams["IBLOCK_SECTION_ID"]
						),
					));

					echo json_encode($this->arResult["SUCCESS"]["ADDED_QUESTION"]);

				}else{
					echo json_encode($this->arResult["ERRORS"]["ADDED_QUESTION_ERROR"]);
				}

				return;
			}


			// fppr($this->arResult, __FILE__.' $this->arResult', false, false);

			$this->includeComponentTemplate();

			
		} catch (Exception $e) {

			$this->errorsFatal[htmlspecialcharsEx($e->getCode())] = htmlspecialcharsEx($e->getMessage());

			echo $e->getMessage();
			
		}
	}
}
?>
