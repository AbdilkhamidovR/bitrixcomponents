<?
namespace Ra\Sale\Offer\Faq;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);


/**
 * Class RaSaleOfferFaq
 * @package Ra\Sale\Offer\Faq
 */
class QuestionFrom{

	static protected $test;

	public function __construct(){
		self::$test = "construct test";
	}

	public function get_test(){
		self::$test = "Question test";
		return self::$test;
	}
}

?>
