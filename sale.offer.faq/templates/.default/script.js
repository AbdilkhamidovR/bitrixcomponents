(function($) {

	"use strict";

	var search_form_el;
	var ajaxUrl;

	/* получение параметров при ajax-запросе с детальной страницы
	/bitrix/templates/aspro_optimus/components/bitrix/catalog.element/main/template.php function initFaqs() */
	setParams();

	$(document).ready(function() {
		
		/* получение параметров со страницы всех вопросов
		/productsfaq */
		setParams();
	})
	
	/**
	 * Выдача результатов поиска
	 */
	.on('click', '.rof-section form.search-form button', function (e) {
		e.preventDefault();
		$('.faq-preload').show();
		// search_form_el = $('.rof-section form.search-form');
		var post_data = search_form_el.serialize();
		$(this).attr("name") == "search_submit" ? post_data += "&search_submit=1" : false;

		if($(this).attr("name") == "search_reset"){
			post_data += "&search_reset=1";
			$(search_form_el).find('input[name="q"]').val("");
		} 
		// console.log(post_data);

		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			success: function(responce){
				// console.log(responce);
				$('.faq-preload').hide();
				print_responce(responce);
			},
			error: function(){
				$('.faq-preload').hide();
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
			}
		});
	})
	

	/**
	 * Нравится - Не нравится
	 */
	.on('click', '.rof-section .rof-item a.like-action', function (e) {
		e.preventDefault();
		$('.faq-preload').show();
		var this_like_el = $(this);
		var this_item_el = $(this).closest(".rof-item");
		var noauthtext = $(this).closest(".rof-likes").data('noauthtext');
		var post_data = "like=" + $(this).data("like") + "&id=" + $(this_item_el).data("id");

		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			dataType: "json",
			success: function(responce){
				// console.log(responce);
				// JSON.parse(responce)
				$('.faq-preload').hide();

				if(responce.USER_ID){
					$(this_item_el).find('.like-result').text(responce.LIKES_QUANTITY);

					if($(this_item_el).find('.like-action.like').hasClass('disabled') || $(this_item_el).find('.like-action.dislike').hasClass('disabled')){
						$(this_item_el).find('.like-action.like').toggleClass('disabled');
						$(this_item_el).find('.like-action.dislike').toggleClass('disabled');
					}else{
						$(this_like_el).addClass('disabled');
					}
				}else{
					show_overlay();
					show_no_auth_popup(noauthtext);
				}
			},
			error: function(){
				$('.faq-preload').hide();
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
			}
		});
	})

	
	/**
	 * Попап-форма Задать вопрос. Открыть
	 */
	.on('click', '.rof-section #add-question-link', function (e) {
		e.preventDefault();
		var noauthtext = $(this).data('noauthtext');
		open_add_question_popup(noauthtext);
	})

	/**
	 * Попап-форма Задать вопрос. Валидация
	 */
	.on('keyup', '.popup-rof-add-question .input-field.question', function (e) {
		if($(this).val().length > 0){
			$(this).closest('.popup-rof-add-question').find('.submit-btn').prop('disabled', false);
		}else{
			$(this).closest('.popup-rof-add-question').find('.submit-btn').prop('disabled', true);
		}
	})


	/**
	 * Закрытие попапов по клику на крестик
	 */
	.on('click', '.x-popup-close', function (e) {
		e.preventDefault();
		close_popup('.popup-rof');
	})
	/**
	 * Закрытие попапов по клику на фон
	 */
	.on('click', '.overlay-rof', function (e) {
		e.preventDefault();
		close_popup('.popup-rof');
	})

	
	/**
	 * Добавление нового вопроса
	 */
	.on('submit', 'form.rof-add-question-form', function (e) {

		e.preventDefault();

		$('.faq-add-preload').show();
		var form_el = $(this);
		var post_data = form_el.serialize() + "&question_submit=1";

		// console.log('ajaxUrl', ajaxUrl);
		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			// dataType: "json",
			success: function(responce){
				// console.log(responce);
				var respObj = JSON.parse(responce);
				$('.faq-add-preload').hide();

				if(respObj.CODE != "ADDED_QUESTION_SUCCESS"){
					$(form_el).find('.input-field.question').addClass('error');
					$(form_el).find('.input-report').html(respObj.MESS).fadeIn(300).addClass('error');
				}else{
					/* Вопрос успешно добавлен */
					$('.popup-rof.popup-rof-add-question').fadeOut(300, function(){
						$(this).find('form.rof-add-question-form .input-field.question').removeClass('error').val("");
						$(this).find('.input-report').removeClass('error').html("").fadeOut(300);

						$('body').append($('.rof-section .popup-rof-add-question-responce'));
						$('.popup-rof-add-question-responce').fadeIn(300);
					});
				}
			},
			error: function(){
				$('.faq-add-preload').hide();
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
			}
		});
	})

	
	/**
	 * Посмотреть еще ответы
	 */
	.on('click', '.rof-section .more-answers-block .more-answers', function (e) {
		e.preventDefault();
		if($(this).hasClass('opened')){
			$(this).next('.more-answers-list').slideUp(300);
		}else{
			$(this).next('.more-answers-list').slideDown(300);
		}
		$(this).toggleClass('opened');
	})
	
	/**
	 * Свернуть доп. ответы
	 */
	.on('click', '.rof-section .more-answers-block .collapse-answers', function (e) {
		e.preventDefault();
		$(this).closest('.more-answers-list').slideUp(300);
		$(this).closest('.more-answers-block').find('.more-answers').toggleClass('opened');
	})


	/**
	 * Выдача результатов для Показать еще вопросы
	 */
	.on('click', '.rof-section .show-more-questions', function (e) {
		e.preventDefault();
		$('.faq-preload').show();
		var this_control_block_el = $(this).closest('.rof-responce-control-block');
		var post_data = $(this).attr('href').replace('?', '');

		if(search_form_el.find('input[name="q"]').val()){
			post_data += '&search_submit=1&q=' + search_form_el.find('input[name="q"]').val();
		}
		// console.log(post_data);

		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			success: function(responce){
				// console.log(responce);
				$('.faq-preload').hide();
				$(this_control_block_el).remove();
				$('.rof-section .rof-responce').append($(responce).find(".rof-block"));
			},
			error: function(){
				$('.faq-preload').hide();
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
			}
		});
	})


	/**
	 * Выдача результатов для Скрыть дополнительные вопросы
	 */
	.on('click', '.rof-section .hide-more-questions', function (e) {
		e.preventDefault();
		$('.faq-preload').show();
		var this_hide_more_el = $(this);
		var post_data = $(this).attr('href').replace('?', '');
		if(search_form_el.find('input[name="q"]').val()){
			post_data += '&search_submit=1&q=' + search_form_el.find('input[name="q"]').val();
		}

		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			success: function(responce){
				print_responce(responce);
				$('.faq-preload').hide();
				$("html,body").animate({"scrollTop":$('.rof-section').offset().top}, "slow");
			},
			error: function(){
				$('.faq-preload').hide();
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
			}
		});
	})
	
	;


	/*
	---------- my functions ---------
	*/

	function setParams(){
		var linked_id = $('.rof-section').attr('id');
		var img_src = $('.rof-section .popup-rof-add-question .input-block img').attr('src');
		var ajaxtarget = $('.rof-section').data("ajaxtarget");
		var limit = $('.rof-section').data("limit");
		//

		search_form_el = $('.rof-section form.search-form');
		ajaxUrl = ajaxtarget + "/?linked_id=" + linked_id + "&img_src=" + img_src + "&limit=" + limit + "&ajax=Y&bitrix_include_areas=N&lang=" + $(search_form_el).attr("lang");
	}

	function print_responce(responce){
		var offer_faq_responce_el = $(responce).find(".rof-responce");
		$('.rof-section .rof-responce').remove();
		$('.rof-section .rof-responce-output').append($(offer_faq_responce_el));
	}

	function open_add_question_popup(noauthtext){
		$('.faq-preload').show();
		// console.log(noauthtext);
		var post_data = "check_auth=1";
		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: post_data,
			dataType: "json",
			success: function(responce){
				// console.log(responce);
				show_overlay();

				if(responce.USER_ID){
					$('body').append($('.rof-section .popup-rof-add-question'));
					// + картинка активного товарного предложения
					var img_src = $('.offers_img img').attr('src');
					$('.popup-rof-add-question .input-block img').attr('src', img_src); 
					//
					$('.popup-rof-add-question').fadeIn(300);
				}else{
					show_no_auth_popup(noauthtext);
				}
				$('.faq-preload').hide();
				return responce.USER_ID;
			},
			error: function(){
				console.log('Error in ajax request');
				console.log('ajaxUrl', ajaxUrl);
				$('.faq-preload').hide();
				return false;
			}
		});
	}


	function show_overlay(){
		$('body').append($('.rof-section .overlay-rof'));
		$('.overlay-rof').fadeIn(300);
		$('body').addClass('fix');
	}
	function show_no_auth_popup(noauthtext){
		$('body').append($('.rof-section .popup-rof-no-auth'));
		$('.popup-rof-no-auth .bodytext').html(noauthtext);
		$('.popup-rof-no-auth').fadeIn(300);
	}

	function close_popup(popup_el){
		$('.overlay-rof').fadeOut(300);
		$(popup_el).fadeOut(300);
		$('body').removeClass('fix');
	}

	// function isValidEmail(email)
	// {
	// 	var mailValid = false;
		
	// 	var testEmail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

	// 	if ( testEmail.test(email) ){
	// 		mailValid = true;
	// 	}else{
	// 		mailValid = false;
	// 	}

	// 	return mailValid;
	// }

	// function form_disable(form){
	// 	form.css({
	// 		'opacity':'0.5',
	// 	});
	// }

	// function form_enable(form){
	// 	form.css({
	// 		'opacity':'1',
	// 	});
	// }


})(jQuery);
