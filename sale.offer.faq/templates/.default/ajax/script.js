(function($) {

	"use strict";

	$(document).ready(function() {
	})
	//toogle menu
	// .on('click', '#', function(event){
	// 	event.preventDefault();
	// })
	;

	$('form.ra-form').submit( function(e){
		e.preventDefault();
		$('.ra-form .ra-form-response .response').fadeOut();

		var form_el = $(this);
		form_disable($(form_el));

		var email_el = $(form_el).find("input[name=EMAIL]");
		var ajaxUrl = $(form_el).attr("action")+"?lang="+$(form_el).attr("lang");

		if( isValidEmail($(email_el).val()) ){
			$(email_el).removeClass('error');

			$.ajax({
				type: "POST",
				url: ajaxUrl,
				data: form_el.serialize(),
				success: function(msg){
					// console.log(msg);

					var answObj = JSON.parse(msg);
					console.log(answObj);

					if(answObj[101] != undefined){
						console.log("Email exists in list \n");
						$('.ra-form .ra-form-response .already-subscribed').fadeIn();
					}else{
						// Successfully subscribed!
						if(answObj["CODE"] == 'subscribed'){
							// dataLayer.push({ 'dl_lang': $(form).attr('lang'), 'dl_form': 'main_page_form', 'event': 'email_form_submit'}); 
							$('.ra-form .ra-form-response .success-subscribed').fadeIn();
						}else{
							$('.ra-form .ra-form-response .other').fadeIn();
							$('.ra-form .ra-form-response .other').html(answObj["CODE"]+answObj["MESSAGE"]);
						}
					}

					form_enable($(form_el));
					// $('input[name=EMAIL]').val('');
				},
				error: function(){
					form_enable($(form_el));
					console.log('Error in ajax request');
				}
			});


		}else{
			form_enable($(form_el));
			$(email_el).addClass('error');
		}
	})

	// $(window).resize(function() {
	// });

	/*
	---------- my functions ---------
	*/
	function isValidEmail(email)
	{
		var mailValid = true;
		
		var testEmail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

		if ( testEmail.test(email) ){
			mailValid = true;
		}else{
			mailValid = false;
		}

		return mailValid;
	}

	function form_disable(form){
		form.css({
			'opacity':'0.5',
		});
	}

	function form_enable(form){
		form.css({
			'opacity':'1',
		});
	}


})(jQuery);
