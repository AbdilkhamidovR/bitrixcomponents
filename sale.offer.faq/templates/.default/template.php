<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $USER;
$isAdmin = $USER->IsAdmin();

?>

<? //fppr($arParams, __FILE__.' arParams');?>
<? //fppr($arResult, __FILE__.' arResult');?>

<section class="rof-section" id="<?=$arParams['LINKED_ID'];?>" 
	data-ajaxtarget="<?=$arParams['PATH_TO_ALL_QUESTIONS'];?>"
	data-limit="<?=$arParams['LIMIT'];?>"
	>

	<div class="faq-preload"></div>
	
	<h3><?=Loc::getMessage("FAQ");?></h3>

	<?/* Поиск */?>
	<? if($arResult["NAV"]["nPages"] > 1):?>
	<div class="rof-search-form">
		<form class="search-form" action="<?=$arParams['PATH_TO_ALL_QUESTIONS'];?>" method="post" accept-charset="utf-8" lang="<?=LANGUAGE_ID;?>">
			<input type="text" name="q" size="15" maxlength="50" placeholder="<?=Loc::getMessage("Q_SEARCH_PLACEHOLDER");?>" value="<?=$arResult["REQUEST"]["SEARCH"] ? $arResult["REQUEST"]["SEARCH"] : '';?>" />
			<input type="hidden" name="ajax" value="Y"  />
			<fieldset>
			<button name="search_submit" class="button transparent grey_br" type="submit" value="1"><?=Loc::getMessage("SEARCH");?></button>
			<button name="search_reset" class="button transparent grey_br" type="submit" value="1"><span class=""><?=Loc::getMessage("RESET");?></span></button>
			</fieldset>
		</form>
	</div>
	<?endif?>
	
	<?/* Вывод найденных ответов */?>
	<div class="rof-responce">

		<? if(count($arResult["ITEMS"]) > 0):?>
			<div class="rof-block" data-pagen="<?=$arResult["NAV"]["iNumPage"];?>">
				<div class="rof-list">
					<?foreach($arResult["ITEMS"] as $arItem):?>
						<div class="rof-item" data-id="<?=$arItem['ID'];?>" data-linked-id="<?=implode(';', $arItem['PROPERTY_Q_LINK_TO_ELEMENT_VALUE']);?>">
							
							<div class="rof-question">
								<div class="title"><?=Loc::getMessage("QUESTION");?></div>
								<div class="content">
									<?=($arItem["PREVIEW_TEXT"] ? $arItem["PREVIEW_TEXT"] : $arItem["NAME"]);?>
									<?if($isAdmin):?>
										<a href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=<?=$arParams["IBLOCK_ID"]?>&amp;type=services&amp;lang=ru&amp;find_section_section=<?=$arParams["IBLOCK_SECTION_ID"]?>&amp;ID=<?=$arItem["ID"]?>" class="adm-edit" >Редактировать</a>
									<?endif;?>
								</div>
							</div>

							<div class="rof-answer">
								<div class="title"><?=Loc::getMessage("ANSWER");?></div>
								<div class="content"><?=$arItem["DETAIL_TEXT"];?></div>
							</div>

							<?if(count($arItem["PROPERTY_Q_MORE_ANSWERS_VALUE"])):?>
								<div class="more-answers-block">
									<a href="#more-answers" class="more-answers"><?=Loc::getMessage("SHOW_MORE_ANSWERS");?> (<?=count($arItem["PROPERTY_Q_MORE_ANSWERS_VALUE"]);?>)</a>
									<ul class="more-answers-list">
										<?foreach ($arItem["PROPERTY_Q_MORE_ANSWERS_VALUE"] as $key => $moreAnswer):?>
										<li class="more-answer-item">
											<div class="more-answer-content">
												<?=$moreAnswer;?>
											</div>
										</li>
										<?endforeach;?>
										<li class="more-answer-control">
											<a href="#collapse-answers" class="collapse-answers"><?=Loc::getMessage("COLLAPSE_MORE_ANSWERS");?></a>
										</li>
									</ul>
								</div>
							<?endif;?>

							<div class="rof-likes" data-noauthtext="<?=Loc::getMessage("NO_AUTH_LIKE_TEXT");?>">
								<span>
									<? $class = $arItem["IS_USER_LIKED_UP"] ? " disabled" : NULL;?>
									<a href="#like-<?=$arItem['ID'];?>" class="like-action like<?=$class;?>" data-like="1"><i class="fa fa-thumbs-up"></i></a>
								</span>
								<span><?=Loc::getMessage("VOTES");?> <span class="like-result"><?=$arItem["PROPERTY_Q_LIKES_QUANTITY_VALUE"];?></span></span>
								<span>
									<? $class = $arItem["IS_USER_LIKED_DOWN"] ? " disabled" : NULL;?>
									<a href="#dislike-<?=$arItem['ID'];?>" class="like-action dislike<?=$class;?>" data-like="-1"><i class="fa fa-thumbs-down"></i></a>
								</span>

							</div>
							
						</div>
					<?endforeach;?>
				</div>
				
				<div class="rof-responce-control-block">
					<div class="rof-add-question">
						<a href="#add-question" id="add-question-link" class="add-question-link button vbig_btn transparent" data-noauthtext="<?=Loc::getMessage("NO_AUTH_ADD_QUESTION_TEXT");?>"><?=Loc::getMessage("ADD_QUESTION");?></a>
					</div>
					<?if($arResult["NAV"]["iNumPage"] < $arResult["NAV"]["nPages"]):?>
					<div class="show-more">
						<a href="?PAGEN_1=<?=$arResult["NAV"]["iNumPage"]+1;?>" class="show-more-questions button vbig_btn transparent grey_br"><?=Loc::getMessage("SHOW_MORE_QUESTIONS");?></a>
					</div>
					<?endif?>
					<?if($arResult["NAV"]["iNumPage"] > 1):?>
					<div class="hide-more">
						<a href="?PAGEN_1=1" class="hide-more-questions button vbig_btn transparent grey_br"><?=Loc::getMessage("HIDE_MORE_QUESTIONS");?></a>
					</div>
					<?endif?>
					
				</div>

			</div>
		<?else:?>
			<div class="rof-responce-footer">
				<p class="rof-search-report no-item-found"><?=Loc::getMessage("NO_ITEM_FOUND");?></p>
				<div class="rof-add-question">
					<a href="#add-question" id="add-question-link" class="add-question-link button vbig_btn transparent" data-noauthtext="<?=Loc::getMessage("NO_AUTH_ADD_QUESTION_TEXT");?>"><?=Loc::getMessage("ADD_QUESTION");?></a>
				</div>
			</div>
		<?endif;?>
	</div>
	<div class="rof-responce-output">
		<!-- ajax responce will be here... -->
	</div>

	<?/* Попап "Задать вопрос" */?>
	<div class="overlay-rof"></div>
	<div class="popup-rof popup-rof-add-question">
		<div class="popup-rof-add-question-header">
			<h2><?=Loc::getMessage("ADD_QUESTION");?></h2>
			<a href="#" class="x-popup-close"><i></i></a>
		</div>
		<div class="popup-rof-add-question-body">
			<form class="rof-add-question-form" action="<?=$arParams['PATH_TO_ALL_QUESTIONS']?>" method="post" accept-charset="utf-8" lang="<?=LANGUAGE_ID;?>">
				<?=bitrix_sessid_post();?>
				<div class="input-block">
					<? if($arParams['IMG_SRC']):?>
						<img src=<?=$arParams['IMG_SRC'];?> alt="" align="left">
					<?endif;?>
					<textarea
						class="input-field question<?=(!$arParams['IMG_SRC'] ? ' full' : '');?>"
						name="question"
						rows="4" cols="50"
						placeholder="<?=Loc::getMessage("QUESTION_PLACEHOLDER");?>"
						required><?=$arResult["REQUEST"]["QUESTION"] ? $arResult["REQUEST"]["QUESTION"] : ""?></textarea>
				</div>
				<div class="input-report">report-area</div>
				<div class="control-block">
					<input name="question_submit" class="submit-btn main-button" type="submit" disabled="disabled" value="<?=Loc::getMessage("ASK");?>" />
				</div>
			</form>
		</div>
		<div class="faq-add-preload"></div>
	</div>


	<?/* Попап "Ваш вопрос успешно добавлен" */?>
	<div class="popup-rof popup-rof-add-question-responce">
		<div class="popup-rof-add-question-header">
			<h2><?=Loc::getMessage("QUESTION_ADDED_SUCCESS_HEADER");?></h2>
			<a href="#" class="x-popup-close"><i></i></a>
		</div>
		<div class="popup-rof-add-question-body">
			<?=Loc::getMessage("QUESTION_ADDED_SUCCESS");?>
		</div>
		<div class="faq-add-preload"></div>
	</div>


	<?/* Попап для неавторизованных */?>
	<div class="popup-rof popup-rof-no-auth">
		<div class="popup-rof-add-question-header">
			<h2><?=Loc::getMessage("NO_AUTH_POPUP_HEADER");?></h2>
			<a href="#" class="x-popup-close"><i></i></a>
		</div>
		<div class="popup-rof-add-question-body">
			<div class="bodytext"></div>
			<p></p>
			<div class="controls">
				<a href="/auth/" class="button transparent">Войти</a>
				<a href="/auth/registration/" class="button transparent">Зарегистрироваться</a>
			</div>
		</div>
		<div class="faq-add-preload"></div>
	</div>


</section>


