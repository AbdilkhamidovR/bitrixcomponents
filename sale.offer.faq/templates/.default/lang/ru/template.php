<?
$MESS["FAQ"] = "Вопросы и ответы клиентов";
$MESS["LIKE"] = "Нравится";
$MESS["VOTES"] = "Голосов:";
$MESS["DISLIKE"] = "Не нравится";
$MESS["SUBMIT"] = "Отправить";
$MESS["SEARCH"] = "Найти";
$MESS["Q_SEARCH_PLACEHOLDER"] = "Введите текст для поиска";
$MESS["RESET"] = "Сбросить";
$MESS["CANCEL"] = "Отменить";
$MESS["QUESTION"] = "Вопрос";
$MESS["ANSWER"] = "Ответ";
$MESS["ASK_QUESTION"] = "Задать вопрос";
$MESS["NO_ITEM_FOUND"] = "Вопросов не найдено";
$MESS["EMAIL_PLACEHOLDER"] = "Ваш email*";
$MESS["QUESTION_PLACEHOLDER"] = "Задайте свой вопрос";
$MESS["QUESTION_ADDED_SUCCESS_HEADER"] = "Ваш вопрос принят";
$MESS["QUESTION_ADDED_SUCCESS"] = "Ваш вопрос был передан специалистам и как только он будет отвечен, мы пришлем ответ вам на почту";
$MESS["ADD_QUESTION"] = "Задать вопрос";
$MESS["ASK"] = "Спросить";
$MESS["SHOW_MORE_ANSWERS"] = "Посмотреть еще ответы на этот вопрос";
$MESS["COLLAPSE_MORE_ANSWERS"] = "Скрыть дополнительные ответы";
$MESS["SHOW_MORE_QUESTIONS"] = "Показать еще вопросы";
$MESS["HIDE_MORE_QUESTIONS"] = "Скрыть дополнительные вопросы";
$MESS["NO_AUTH_POPUP_HEADER"] = "Вы не авторизованы";
$MESS["NO_AUTH_ADD_QUESTION_TEXT"] = "Чтобы задать интересующий Вас вопрос по продукции OLEXDECO, пожалуйста, авторизуйтесь на сайте – ответ придёт на указанный Вами при регистрации адрес электронной почты";
$MESS["NO_AUTH_LIKE_TEXT"] = "Чтобы оценить полезность заданного вопроса, пожалуйста, зарегистрируйтесь на сайте – для составления честного рейтинга голосование доступно только для авторизованных пользователей";
?>