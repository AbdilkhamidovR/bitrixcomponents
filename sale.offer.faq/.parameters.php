<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (!CModule::IncludeModule('iblock')) die("Модуль iblock не установлен");

$arIBlock = array();
$iblockFilter = (array('ACTIVE' => 'Y'));
$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
unset($arr, $rsIBlock, $iblockFilter);

$rsSections = CIBlockSection::GetList(
	array("SORT" => "ASC"), 
	array("IBLOCK_ID" => $arCurrentValues['IBLOCK_ID'], "ACTIVE" => "Y"), 
	false, 
	array("IBLOCK_ID", "ID", "NAME")
);
while ($arr = $rsSections->Fetch())
	$arSections[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"IBLOCK_SECTION_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_IBLOCK_SECTION_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arSections,
			"REFRESH" => "Y",
		),
		"LINKED_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_LINKED_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"LIMIT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => "6",
		),
		"IMG_SRC" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_IMG_SRC"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"PATH_TO_ALL_QUESTIONS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SOF_PATH_TO_ALL_QUESTIONS"),
			"TYPE" => "STRING",
			"DEFAULT" => "/productsfaq/",
		),
	),
);
?>
