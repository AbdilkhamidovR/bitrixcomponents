<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (!CModule::IncludeModule('iblock')) die("Модуль iblock не установлен");

// $arIBlock = array();
// $iblockFilter = (array('ACTIVE' => 'Y'));
// $rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
// while ($arr = $rsIBlock->Fetch())
// 	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
// unset($arr, $rsIBlock, $iblockFilter);

// $rsSections = CIBlockSection::GetList(
// 	array("SORT" => "ASC"), 
// 	array("IBLOCK_ID" => $arCurrentValues['IBLOCK_ID'], "ACTIVE" => "Y"), 
// 	false, 
// 	array("IBLOCK_ID", "ID", "NAME")
// );
// while ($arr = $rsSections->Fetch())
// 	$arSections[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];

$arDiscounts = array();

$arComponentParameters = array(
	"PARAMETERS" => array(
		// "IBLOCK_ID" => array(
		// 	"PARENT" => "BASE",
		// 	"NAME" => GetMessage("CBS_IBLOCK_ID"),
		// 	"TYPE" => "LIST",
		// 	"VALUES" => $arIBlock,
		// 	"REFRESH" => "Y",
		// ),
		"DISCOUNT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_DISCOUNT_ID"),
			"TYPE" => "STRING",
			"VALUES" => "28",
			"REFRESH" => "Y",
		),
		"PHONE_FIELD_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_PHONE_FIELD_CODE"),
			"TYPE" => "STRING",
			"VALUES" => "CBS_PHONE",
			"REFRESH" => "Y",
		),
		"FORM_STATUS_READY" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_FORM_STATUS_READY"),
			"TYPE" => "STRING",
			"VALUES" => "COUPON_CREATED",
			"REFRESH" => "Y",
		),
		"EMAIL_FIELD_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_EMAIL_FIELD_CODE"),
			"TYPE" => "STRING",
			"VALUES" => "CBS_EMAIL",
			"REFRESH" => "Y",
		),
		"COUPON_ID_FIELD_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_COUPON_ID_FIELD_CODE"),
			"TYPE" => "STRING",
			"VALUES" => "CBS_COUPON_ID",
			"REFRESH" => "Y",
		),
		"COUPON_CODE_FIELD_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_COUPON_CODE_FIELD_CODE"),
			"TYPE" => "STRING",
			"VALUES" => "CBS_COUPON_ID",
			"REFRESH" => "Y",
		),
		"IP_FIELD_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CBS_IP_FIELD_CODE"),
			"TYPE" => "STRING",
			"VALUES" => "CBS_COUPON_ID",
			"REFRESH" => "Y",
		),
	),
);
?>
