<?

namespace Ra\Coupon\By\Subcribe;

use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\UserTable as UserTable;
use Bitrix\Sale\Internals\DiscountCouponTable as DiscountCouponTable;
use Bitrix\Main\Mail\Event as Event;

Loader::includeModule('form');
Loader::includeModule('bxmaker.smsnotice'); 

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


Loc::loadMessages(__FILE__);

// error_reporting(E_ERROR);

/**
 * Class RaCouponBySubcribe
 * @package Ra\Coupon\By\Subcribe
 */
class RaCouponBySubcribe extends \CBitrixComponent{

	public function onPrepareComponentParams($arParams){
		$this->arResult["SUCCESS"] = array();
		$this->arResult["ERRORS"] = array();
		return $arParams;
	}


	/**
	 * Обработка запроса
	 * 
	 */
	protected function processRequest(){
		$this->arResult["REQUEST"] = array(
			"BASE_URL" => $this->request->getRequestedPageDirectory(),
			"WEB_FORM_ID" => $this->request->getQuery("WEB_FORM_ID"),
			"RESULT_ID" => $this->request->getQuery("RESULT_ID"),
			// "POST_LIST" => $this->request->getPostList(),
			"METHOD" => $this->request->getRequestMethod(),
			"IS_AJAX" => $this->request->isAjaxRequest(),
		);
		return;
	}

	/**
	 * Получение результата заполнения формы
	 * 
	 */
	protected function readFormData(){
		\CForm::GetResultAnswerArray($this->arResult["REQUEST"]["WEB_FORM_ID"], 
			$arFields, 
			$arrAnswers, 
			$arrAnswers2, 
			array("RESULT_ID" => $this->arResult["REQUEST"]["RESULT_ID"])
		);

		$this->arResult["FORM_ROW"] = \CFormResult::GetByID($this->arResult["REQUEST"]["RESULT_ID"])->Fetch();

		foreach ($arrAnswers2[$this->arResult["REQUEST"]["RESULT_ID"]] as $key => $arAnswer) {
			$this->arResult["FORM_DATA"][$arAnswer[0]["SID"]] = $arAnswer[0];
		}

		$this->arResult["FORM_STATUS_READY"] = \CFormStatus::GetList(
			$this->arResult["REQUEST"]["WEB_FORM_ID"],
			$by="s_id",
			$order="desc",
			array(
				"TITLE" => $this->arParams["FORM_STATUS_READY"]
			),
			$is_filtered
		) -> Fetch();

	}


	/**
	 * 
	 */
	protected function normalizePhone(){
		$this->arResult["FORM_DATA"][$this->arParams["PHONE_FIELD_CODE"]]["USER_TEXT"] = NormalizePhone($this->arResult["FORM_DATA"][$this->arParams["PHONE_FIELD_CODE"]]["USER_TEXT"]);
		$this->writeFormData($this->arParams["PHONE_FIELD_CODE"], $this->arResult["FORM_DATA"][$this->arParams["PHONE_FIELD_CODE"]]["USER_TEXT"]);
	}


	/**
	 * 
	 */
	protected function writeFormData($fieldCode, $value){
		\CFormResult::SetField(
			$this->arResult["REQUEST"]["RESULT_ID"],
			$fieldCode,
			array($this->arResult["FORM_DATA"][$fieldCode]["ANSWER_ID"] => $value)
		);
	}


	/**
	 * Получение id пользователя-создателя купонов
	 * 
	 */
	protected function getCreatorId(){
		$arCreator = UserTable::getRow(array(
			'filter' => array('=LOGIN' => "bitrix"),
			'select' => array('ID')
		));
		return $arCreator["ID"];
	}


	/**
	 * Проверка уникальности нового пользователя по полю Телефон
	 * 
	 */
	protected function isExist($phone){
		if($phone){
			$arFilter = array("STATUS_ID" => 2);
			$arFields = array();
			$arFields[] = array(
				"SID" => $this->arParams["PHONE_FIELD_CODE"],
				"FILTER_TYPE" => "text",
				"PARAMETER_NAME" => "USER",
				"VALUE" => $phone,
				"EXACT_MATCH" => "Y"
			);
			$arFilter["FIELDS"] = $arFields;

			$rs = \CFormResult::GetList(
				$this->arResult["REQUEST"]["WEB_FORM_ID"],
				($by="s_timestamp"),
				($order="desc"),
				$arFilter,
				$is_filtered,
				"N",
				false
			);
			return (boolean)$rs->SelectedRowsCount();
		}else{
			$this->arResult["ERRORS"][] = "Не получен телефон";
			return false;
		}
	}

	/**
	 * Удаление результата формы
	 * 
	 */
	protected function deleteFormRow($id){
		if($this->arResult["FORM_ROW"]["STATUS_TITLE"] != $this->arParams["FORM_STATUS_READY"]){
			\CFormResult::Delete($id, "N");
		}
	}

	/**
	 * Создание купона
	 * 
	 */
	protected function createCoupon(){

		if(!(int)$this->arParams["DISCOUNT_ID"]){
			$this->arResult["ERRORS"][] = "Не получен id правила корзины";
			return false;
		}

		$this->arResult["COUPON"]["FIELDS"] = array(
			'DISCOUNT_ID' => $this->arParams["DISCOUNT_ID"],
			'COUPON' => DiscountCouponTable::generateCoupon(),
			'ACTIVE_FROM' => null, // без ограничения к началу даты активности купона
			'ACTIVE_TO' => null, // без ограничения к окончанию даты активности купона
			'CREATED_BY' => $this->getCreatorId(),	// создатель купона             
			'MODIFIED_BY' => $this->getCreatorId(),                
			'TYPE' => DiscountCouponTable::TYPE_MULTI_ORDER, // TYPE_MULTI_ORDER - использовать на несколько заказов 
		);

		// проверка входных данных для создания купона
		$checkResult = DiscountCouponTable::checkPacket($this->arResult["COUPON"]["FIELDS"], true);
		if (!$checkResult->isSuccess()){
			foreach ($checkResult->getErrors() as $checkError){
				$this->arResult["ERRORS"][] = $checkError;
			}
			return false;
		}

		// create coupon
		$addResult = DiscountCouponTable::add($this->arResult["COUPON"]["FIELDS"]);

		if (!$addResult->isSuccess()){
			$this->arResult["ERRORS"][] = $addResult->getErrorMessages();
		   return false;
		}else{
			// coupon succesfully created!
			DiscountCouponTable::enableCheckCouponsUse();
			DiscountCouponTable::updateUseCoupons();
			$this->arResult["COUPON"]["ID"] = $addResult->getId();
			return $this->arResult["COUPON"]["ID"];
		}
	}


	


	/**
	 * Execute component
	 * 
	 */
	public function executeComponent(){

		try {
			$this->processRequest();
			if($this->arResult["REQUEST"]["WEB_FORM_ID"] && $this->arResult["REQUEST"]["RESULT_ID"]){
				
				$this->readFormData();

				if($this->arResult["FORM_ROW"]){

					$this->normalizePhone();
					
					$phone = $this->arResult["FORM_DATA"][$this->arParams["PHONE_FIELD_CODE"]]["USER_TEXT"];
					if($this->isExist($phone)){
						$this->arResult["ERRORS"][] = "Пользователь с таким телефоном (".$phone.") уже существует";
						$this->deleteFormRow($this->arResult["REQUEST"]["RESULT_ID"]);
					}else{

						if($couponId = $this->createCoupon()){
							// Купон успешно добавлен!
							$this->writeFormData($this->arParams["COUPON_ID_FIELD_CODE"], $couponId);
							$this->writeFormData($this->arParams["COUPON_CODE_FIELD_CODE"],  $this->arResult["COUPON"]["FIELDS"]["COUPON"]);
							\CFormResult::SetStatus($this->arResult["REQUEST"]["RESULT_ID"], $this->arResult["FORM_STATUS_READY"]["ID"], "N");

							$email = $this->arResult["FORM_DATA"][$this->arParams["EMAIL_FIELD_CODE"]]["USER_TEXT"];
							// send email
							if($email){
								Event::send(array(
									"EVENT_NAME" => "CBS_FORM_COUPON_CREATED",
									"LID" => "s1",
									"C_FIELDS" => array(
										"RS_USER_PHONE" => $phone,
										"RS_USER_EMAIL" => $email,
										"RS_USER_IP" => $this->arResult["FORM_DATA"][$this->arParams["IP_FIELD_CODE"]]["USER_TEXT"],
										"RS_COUPON_ID" => $couponId,
										"RS_COUPON_CODE" => $this->arResult["COUPON"]["FIELDS"]["COUPON"]
									),
								));
							}

							// send sms
							if($phone){
								$oManager = \Bxmaker\SmsNotice\Manager::getInstance();
								$result = $oManager->send($phone, 'Код вашего купона на скидку 25% на сайте '.SITE_SERVER_NAME.': '.$this->arResult["COUPON"]["FIELDS"]["COUPON"]);

								$this->arResult["SUCCESS"][] = "Купон (".$this->arResult["COUPON"]["FIELDS"]["COUPON"].") успешно добавлен";
							}
						}

					}
				}else{
					$this->arResult["ERRORS"][] = "Результат (#".$this->arResult["REQUEST"]["RESULT_ID"].") заполнения веб-формы не найден";
				}
				
			}
			
			// fppr($this->arParams, __FILE__.' $this->arParams');
			fppr($this->arResult, __FILE__.' $this->arResult');

			$this->includeComponentTemplate();

			return $couponId;

			
		} catch (Exception $e) {

			$this->errorsFatal[htmlspecialcharsEx($e->getCode())] = htmlspecialcharsEx($e->getMessage());

			echo $e->getMessage();
			
		}
	}
}
?>
