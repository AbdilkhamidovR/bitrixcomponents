<?
$MESS["SRCH_STR_NOT_ALLOWED"] = "Недопустимый ввод в форме поиска (недопустимые символы или длина строки меньше трех)";
$MESS["SRCH_STRLEN_SHORT"] = "Длина строки поиска меньше трех";
$MESS["VL_EMAIL_NOT_VALID"] = "Ошибка ввода email";
$MESS["VL_QUESTION_NOT_VALID"] = "Ошибка ввода вопроса (нужно больше 5 символов)";
$MESS["ADDED_QUESTION_ERROR"] = "Ошибка добавления вопроса";
$MESS["ADDED_QUESTION_SUCCESS"] = "Ваш вопрос принят. Мы ответим в ближайшее время";
?>