<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// use Bitrix\Main\Localization\Loc;
// Loc::loadMessages(__FILE__);

?>

<? //ppr($arParams, __FILE__.' arParams');?>
<? //ppr($arResult, __FILE__.' arResult');?>

<?if(count($arResult["ERRORS"])):?>
	<p><font class="errortext"><?=implode("<br />", $arResult["ERRORS"]);?></font></p>
<?else:?>
	<p><font class="notetext"><?=implode("<br />", $arResult["SUCCESS"]);?></font></p>
<?endif;?>


<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"couponsbysubscribe",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "couponsbysubscribe",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "1",
	)
);?>

