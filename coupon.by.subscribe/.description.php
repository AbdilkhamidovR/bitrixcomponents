<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CBS_NAME"),
	"DESCRIPTION" => GetMessage("CBS_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "RA",
		"CHILD" => array(
			"ID" => "sale",
			"NAME" => GetMessage("CBS_CHILD_NAME"),
			"SORT" => 30
		),
	),
);

?>